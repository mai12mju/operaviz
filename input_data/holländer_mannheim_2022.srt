1
00:01:44,599 --> 00:01:49,839
Steuermann, lass die Wacht! Steuermann, her zu uns!

2
00:01:49,840 --> 00:01:55,320
Ho! He! Je! Ha! Hisst die Segel auf! Anker fest! Steuermann, her!

3
00:01:57,480 --> 00:02:02,480
Fürchten weder Wind noch bösen Strand, wollen heute mal recht lustig sein!

4
00:02:02,480 --> 00:02:07,640
Jeder hat sein Mädel auf dem Land, herrlichen Tabak und guten Branntewein!

5
00:02:07,640 --> 00:02:12,479
Hussassahe! Klipp und Sturm drauß … Jolohohe! … lachen wir aus!

6
00:02:12,479 --> 00:02:20,039
Hussassahe! Segel ein! Anker fest! Klipp und Sturm lachen wir aus!

7
00:02:20,319 --> 00:02:25,599
Steuermann, lass die Wacht! Steuermann, her zu uns!

8
00:02:25,599 --> 00:02:30,840
Ho! He! Je! Ha! Steuermann, her! Trink mit uns!

9
00:02:30,840 --> 00:02:38,520
Ho! He! Je! Ha! Klipp und Sturm, he! Sind vorbei, he! Hussahe!

10
00:02:38,520 --> 00:02:43,360
Hallohe! Steuermann! Ho! Her, komm und trink mit uns!

11
00:03:03,960 --> 00:03:08,960
Mein, seht doch an!

12
00:03:08,960 --> 00:03:16,800
Sie tanzen gar! Der Mädchen bedarf's da nicht, fürwahr!

13
00:03:16,800 --> 00:03:21,280
He! Mädel! Halt! Wo geht ihr hin?

14
00:03:21,280 --> 00:03:31,199
Steht euch nach frischem Wein der Sinn? Eu'r Nachbar dort soll auch was haben!

15
00:03:31,800 --> 00:03:36,160
Ist Trank und Speis für euch allein?

16
00:03:36,159 --> 00:03:47,120
Fürwahr! Tragt's hin den armen Knaben, vor Durst sie scheinen matt zu sein.

17
00:03:47,120 --> 00:03:50,439
Man hört sie nicht.

18
00:03:50,439 --> 00:03:56,639
Ei seht doch nur! Kein Licht, von der Mannschaft keine Spur!

19
00:03:56,639 --> 00:04:02,119
He! Seeleut! He! Wollt Fackeln ihr?

20
00:04:02,120 --> 00:04:06,360
Wo seid ihr doch? Man sieht nicht hier!

21
00:04:07,080 --> 00:04:11,760
Weckt sie nicht auf! Sie schlafen noch!

22
00:04:11,759 --> 00:04:16,959
He! Seeleut! He! Antwortet doch!

23
00:04:25,600 --> 00:04:34,720
Wahrhaftig, sie sind tot, sie haben Speis und Trank nicht not!

24
00:04:34,720 --> 00:04:38,280
Ei Seeleute, liegt ihr so faul schon im Nest?

25
00:04:38,279 --> 00:04:42,359
Ist heute für euch denn nicht auch ein Fest?

26
00:04:42,360 --> 00:04:50,520
Sie liegen fest auf ihrem Platz, wie Drachen hüten sie den Schatz.

27
00:04:50,519 --> 00:04:58,560
He Seeleute, wollt ihr nicht frischen Wein? Ihr müsset wahrlich doch durstig auch sein!

28
00:04:58,560 --> 00:05:08,040
Sie trinken nicht, sie singen nicht, in ihrem Schiffe brennt kein Licht.

29
00:05:08,040 --> 00:05:12,400
Sagt, habt ihr denn nicht auch ein Schätzchen am Land?

30
00:05:12,399 --> 00:05:15,719
Wollt ihr nicht mit tanzen auf freundlichem Strand?

31
00:05:15,720 --> 00:05:24,960
Sie sind schon alt und bleich statt rot, und ihre Liebsten, die sind tot.

32
00:05:24,959 --> 00:05:33,519
He! Seeleut! Wacht doch auf! Wir bringen euch Speis und Trank zu Hauf!

33
00:05:33,519 --> 00:05:40,240
Seeleut! Wacht doch auf!

34
00:05:44,480 --> 00:05:50,120
Seeleut! Wacht doch auf!

35
00:06:01,639 --> 00:06:13,319
Wahrhaftig, ja! Sie scheinen tot! Sie haben Speis und Trank nicht not.

36
00:06:13,319 --> 00:06:21,000
Vom fliegenden Holländer wisst ihr, sein Schiff, wie es leibt und lebt, seht ihr da!

37
00:06:21,000 --> 00:06:30,439
So weckt die Mannschaft ja nicht auf, Gespenster sind's, wir schwören drauf!

38
00:06:30,439 --> 00:06:34,199
Wieviel hundert Jahre schon seid ihr zur See?

39
00:06:34,199 --> 00:06:38,159
Euch tut ja der Sturm und die Klippe nicht weh!

40
00:06:38,160 --> 00:06:47,280
Sie trinken nicht, sie singen nicht, in ihrem Schiffe brennt kein Licht.

41
00:06:47,279 --> 00:06:51,079
Habt ihr keine Brief, keine Aufträg fürs Land?

42
00:06:51,079 --> 00:06:55,079
Unsern Urgroßvätern wir bringen's zur Hand!

43
00:06:55,079 --> 00:07:04,439
Sie sind schon alt und bleich statt rot, und ihre Liebsten, ach! Sind tot!

44
00:07:04,439 --> 00:07:16,560
Hei! Seeleute, spannt eure Segel auf und zeigt uns des fliegenden Holländers Lauf!

45
00:07:16,560 --> 00:07:22,280
Sie hören nicht, uns graust es hier!

46
00:07:22,279 --> 00:07:26,159
Sie wollen nichts, was rufen wir?

47
00:07:27,319 --> 00:07:37,560
Ihr Mädel, lasst die Toten ruhn! Lasst sie ruhn! Lasst's uns Lebend'gen gütlich tun!

48
00:07:38,439 --> 00:07:49,000
– So nehmt, der Nachbar hat's verschmäht! – Wie? Kommt ihr denn nicht selbst an Bord?

49
00:07:49,000 --> 00:07:56,199
Ei, jetzt noch nicht, es ist ja nicht spät. Wir kommen bald, jetzt trinkt nur fort!

50
00:07:56,199 --> 00:08:12,240
Und, wenn ihr wollt, so tanzt dazu, nur gönnt dem müden Nachbar Ruh, lasst ihm Ruh!

51
00:08:14,279 --> 00:08:19,519
Juchhe! Da gibt's die Fülle!

52
00:08:19,519 --> 00:08:23,079
Lieb Nachbar, habe Dank!

53
00:08:23,079 --> 00:08:26,159
Zum Rand sein Glas ein jeder fülle!

54
00:08:28,000 --> 00:08:33,039
Lieb Nachbar liefert uns den Trank!

55
00:08:34,919 --> 00:08:43,799
Lieb Nachbarn, habt ihr Stimm und Sprach, so wachet auf und macht's uns nach!

56
00:08:44,679 --> 00:08:53,479
Wachet auf! Auf, macht's uns nach!

57
00:08:53,480 --> 00:08:58,600
– Steuermann, lass die Wacht! Her zu uns! – Auf, lass die Wacht! Komm her zu uns!

58
00:08:58,600 --> 00:09:03,960
Ho! He! Hisst die Segel auf! Anker fest! Steuermann, her!

59
00:09:06,480 --> 00:09:11,320
Wachten manche Nacht bei Sturm und Graus, tranken oft des Meers gesalznes Nass.

60
00:09:11,320 --> 00:09:16,560
Heute wachen wir bei Saus und Schmaus, besseres Getränk gibt Mädel uns vom Fass!

61
00:09:16,559 --> 00:09:21,639
Hussassahe! Klipp und Sturm drauß … Jollolohe! … lachen wir aus!

62
00:09:21,639 --> 00:09:28,960
Hussassahe! Segel ein! Anker fest! Klipp und Sturm lachen wir aus!

63
00:09:28,960 --> 00:09:34,519
– Steuermann, lass die Wacht! Her zu uns! – Auf, lass die Wacht! Komm her zu uns!

64
00:09:34,519 --> 00:09:45,000
Ho! He! Steuermann, her! Trink mit uns!

65
00:09:45,000 --> 00:09:52,200
Hussahe! Hallohe! Steuermann! Ho! Her, komm und trink mit uns!

