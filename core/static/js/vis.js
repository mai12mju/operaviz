// import * as d3 from "https://cdn.jsdelivr.net/npm/d3@7/+esm";

var margin = ({top: 50, right: 50, bottom: 50, left: 50});
var width = 1000 + margin.left + margin.right;
var height = 300 + margin.top + margin.bottom;
var barHeigth = 270;

console.log(width);
console.log(barcodeUrl);
console.log(videoUrl);
console.log(videoLength);
console.log(marks);

var svg = d3.select('#vis').append('svg').attr("width", width).attr("height", height);
    //.attr("viewBox", [0, 0, width, height]);

svg.append('svg:image')
.attr('xlink:href', barcodeUrl).attr("width", 1000).attr("x", margin.left).attr("y", margin.top);

var barMargins = {};
var keys = [];
for (var index in marks) {
    var data = marks[index];
    var barX = (data.start_time / videoLength * 1000);
    var barY = barX + 10;

    data.barX = barX;
    data.barY = barY;
    barMargins[[barX, barY]] = data;
    keys.push([barX, barY]);
}

keys.sort();
console.log(keys);

var usedKeys = [];
var clusters = [];
for (i = 0; i < keys.length; i++) {
    if (usedKeys.includes(keys[i])) {
        continue;
    }
    usedKeys.push(keys[i]);
    var cluster = [keys[i]];
    var upperBound = keys[i][1];
    for (j = i + 1; j < keys.length; j++) {
        if (usedKeys.includes(keys[j]) == false) {
            if (keys[j][0] > keys[i][0] && keys[j][0] < upperBound) {
                cluster.push(keys[j]);
                upperBound = keys[j][1];
                usedKeys.push(keys[j]);
            }
        }
    }
    clusters.push(cluster);
}
console.log(clusters);

for (var i in clusters) {
    var cluster = clusters[i];
    var x1Values = [];
    var x2Values = [];
    var dataPoints = [];
    for (var j in cluster) {
        x1Values.push(cluster[j][0]);
        x2Values.push(cluster[j][1]);
        dataPoints.push(barMargins[cluster[j]]);
    }
    drawBar(dataPoints, Math.min(...x1Values), Math.max(...x2Values), svg)
}

var videoPlayer = document.getElementById("video-player");
var hls = new Hls({
            debug: true,
            autoStartLoad: false
          });

hls.loadSource(masterM3u8Url);
hls.attachMedia(videoPlayer);


function drawBar(dataPoints, x1Value, x2Value, svg) {
    x1Value = x1Value + margin.left;
    x2Value = x2Value + margin.left;

    svg.append("rect").attr("fill", "red").attr("opacity", 0.4)
        .attr("x", x1Value).attr("y", margin.top - 10).attr("height", barHeigth).attr("width", x2Value - x1Value - 5)
        .on("mouseover", (event, d) => tooltipMouseIn(event, d, x1Value, x2Value, dataPoints))
        .on("mouseout", (event, d) => tooltipMouseOut(event, d, x1Value, x2Value, dataPoints))
        .on("click", (event, d) => resultsClick(event, d, x1Value, x2Value, dataPoints));
}

function tooltipMouseIn(event, d, x1Value, x2Value, dataPoints) {
    d3.select(event.target).attr("opacity", 0.8);

    var tooltip = svg.append("g").attr("id", "tooltip");
    var ttX = event.clientX - 400;
    var ttY = event.clientY - 250;
    console.log(ttY);
    console.log(height);
    if (ttX > width / 2) {
        ttX = ttX - 420;
    }
    if (ttY > height / 2) {
        ttY = ttY - (dataPoints.length * 28 + 2) - 20;
    }
    console.log(ttY);
    tooltip.append("rect")
        .attr("fill", "gray")
        .attr("opacity", 1.0)
        .attr("width", 400)
        .attr("height", dataPoints.length * 28 + 2)
        .attr("x", ttX)
        .attr("y", ttY);

    var infoY = ttY + 14;
    for (var i in dataPoints) {
        tooltip.append("circle").attr("fill", "black").attr("cx", ttX + 5).attr("cy", infoY).attr("r", 3);
        tooltip.append("text").attr("fill", "black").attr("font-size", 12).attr("x", ttX + 12).attr("y", infoY)
            .text(convertTime(dataPoints[i].start_time) + " - " + convertTime(dataPoints[i].end_time) + ":");
        tooltip.append("text").attr("fill", "black").attr("font-size", 12).attr("x", ttX + 12).attr("y", infoY + 12)
            .text(dataPoints[i].text);
        infoY = infoY + 28;
    }
}

function tooltipMouseOut(event, d, x1Value, x2Value, dataPoints) {
    if (d3.select(event.target).attr("class") != "clicked") {
        d3.select(event.target).attr("opacity", 0.4);
    }
    d3.select("#tooltip").remove();
}

function resultsClick(event, d, x1Value, x2Value, dataPoints) {
    var resultsDiv = document.getElementById("results");
    console.log(d3.select(event.target).attr("class"));
    if (d3.select(event.target).attr("class") == "clicked") {
        d3.select(event.target).attr("class", "").attr("fill", "red");
        resultsDiv.querySelectorAll('*').forEach(n => n.remove());
        return;
    }
    d3.selectAll(".clicked").attr("fill", "red").attr("opacity", 0.4).attr("class", "");
    d3.select(event.target).attr("opacity", 0.8).attr("fill", "yellow").attr("class", "clicked");
    resultsDiv.querySelectorAll('*').forEach(n => n.remove());
    for (var i in dataPoints) {
        var resultDiv = document.createElement("div");
        resultDiv.className = "result-container";
        var timeSpan = document.createElement("span");
        timeSpan.appendChild(document.createTextNode(convertTime(dataPoints[i].start_time) + " - " + convertTime(dataPoints[i].end_time) + ":"));
        timeSpan.className = "time-span";
        var textSpan = document.createElement("span");
        textSpan.appendChild(document.createTextNode(dataPoints[i].text));
        textSpan.className = "text-span";
        var playDiv = document.createElement("div");
        playDiv.className = "play-div";
        var playButton = document.createElement("button");
        playButton.appendChild(document.createTextNode("Play"));
        playButton.type = "button";
        playButton.className = "play-button";
        playButton.onclick = function(startTime) { return () => playVideo(startTime); }(dataPoints[i].start_time);
        playDiv.appendChild(playButton);
        resultDiv.appendChild(timeSpan);
        resultDiv.appendChild(textSpan);
        resultDiv.appendChild(playDiv);

        resultsDiv.appendChild(resultDiv);
    }
}

function playVideo(startTime) {
    var videoPlayer = document.getElementById("video-player");
    hls.stopLoad();
    hls.startLoad(startTime);
    videoPlayer.currentTime = startTime;
    videoPlayer.play();
}

function convertTime(time) {
    var seconds = Math.floor(time % 60);
    seconds = styleDigits(seconds);
    time /= 60;
    var minutes = Math.floor(time % 60);
    minutes = styleDigits(minutes)
    time /= 60;
    var hours = Math.floor(time % 60);
    hours = styleDigits(hours)

    return hours + ":" + minutes + ":" + seconds;
}

function styleDigits(num) {
    num = num.toString();
    if (num.length == 1) {
        return "0" + num;
    }
    return num;
}