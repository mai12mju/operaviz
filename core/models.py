from django.db import models


class Document(models.Model):
    start_time = models.IntegerField()
    end_time = models.IntegerField()
    text = models.TextField()
    video = models.ForeignKey("Video", on_delete=models.CASCADE)


class Token(models.Model):
    name = models.TextField()
    documents = models.ManyToManyField("Document")


class Video(models.Model):
    name = models.TextField(unique=True)
    barcode_image = models.ImageField(upload_to="barcodes/", null=True)
    barcode_json = models.JSONField(null=True)
    video_file = models.FileField(upload_to="videos/")
    m3u8_master = models.FileField(upload_to="videos/", null=True)
    total_length = models.IntegerField(null=True)

