from django.shortcuts import render

from core.helper import search
from core.models import Video


def index(request):
    print(request.POST)

    search_string = request.POST.get("search_string", "")
    video = Video.objects.get(name="holländer_mannheim_2022")
    results = search(search_string, video)
    print(results)
    return render(request, "index.html", context={"results": results})
