import spacy
import de_core_news_sm
import cv2

from core.models import Document, Token


def search(search_string, video):
    nlp = de_core_news_sm.load()
    doc = nlp(search_string)

    query_tokens = []
    for spacy_token in doc:
        if spacy_token.lemma_ != "--" and spacy_token.text != "\n":
            query_tokens.append(spacy_token.lemma_.lower())

    print(query_tokens)
    tokens = Token.objects.filter(name__in=query_tokens)
    documents = []
    for token in tokens:
        documents.append(set(token.documents.filter(video=video)))

    rel_documents = set()
    if documents:
        rel_documents = set.intersection(*documents)

    for doc in rel_documents:
        print(doc.text)

    return_dict = {
        "search_string": search_string,
        "video_length": video.total_length,
        "barcode_image": video.barcode_image.url,
        "video_file": video.video_file.url,
        "video_name": video.name,
        "master_m3u8": video.m3u8_master.url,
        "documents": []
    }
    for doc in rel_documents:
        return_dict["documents"].append({
            "start_time": doc.start_time,
            "end_time": doc.end_time,
            "text": doc.text
        })

    return return_dict


def count_frames(path, override=False):
    video = cv2.VideoCapture(path)

    if override:
        total_frames = count_frames_manual(video)

    else:
        try:
            total_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))

            if total_frames < 0:
                total_frames = count_frames_manual(video)
        except:
            total_frames = count_frames_manual(video)

    video.release()

    return total_frames


def count_frames_manual(video):
    total_frames = 0

    while True:
        (grabbed, frame) = video.read()

        if not grabbed:
            break

        total_frames += 1

    return total_frames
