import logging
from pathlib import Path
import subprocess
import shlex

import pysrt
import spacy
import de_core_news_sm

from django.core.management.base import BaseCommand
from django.core.files import File
from django.core.files.base import ContentFile
from django.conf import settings

from core.models import Document, Token, Video

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Reads an subtitle file"

    def add_arguments(self, parser):
        parser.add_argument("-s", "--sub", required=True, help="path to subtitle file")
        parser.add_argument("--video", required=True, help="path of video file")

    def handle(self, *args, **options):
        sub_path = options["sub"]
        video_path = options["video"]

        print("Save video in db...")
        with open(video_path, mode="rb") as video_file:
            video_file = File(video_file, name=Path(video_path).name)
            video = Video.objects.create(name=Path(video_path).stem)
            video.video_file = video_file
            video.save()

        print("Convert subtitles to vtt...")
        subs = pysrt.open(sub_path)
        vtt_path = convert_to_vtt(subs, sub_path)
        prepare_video_for_hls(video, vtt_path)

        print("Load spacy model...")
        nlp = de_core_news_sm.load()

        print("Create tokens...")
        for sub in subs:
            text = sub.text
            start_seconds = sub.start.hours * 3600 + sub.start.minutes * 60 + sub.start.seconds
            end_seconds = sub.end.hours * 3600 + sub.end.minutes * 60 + sub.end.seconds

            document = Document.objects.create(text=text, start_time=start_seconds, end_time=end_seconds, video=video)

            doc = nlp(text)
            for spacy_token in doc:
                if spacy_token.lemma_ != "--" and spacy_token.text != "\n":
                    token, _ = Token.objects.get_or_create(name=spacy_token.lemma_.lower())
                    token.documents.add(document)


def convert_to_vtt(subs, video_path):
    vtt_path = settings.MEDIA_ROOT + "videos/" + Path(video_path).stem + ".vtt"
    with open(vtt_path, mode="w") as vtt_file:
        vtt_file.write("WEBVTT\n\n")
        print(subs[0])
        print(subs[0].index, subs[0].start, subs[0].end)
        for sub in subs:
            vtt_file.write(str(sub.index) + "\n")
            vtt_file.write(str(sub.start).replace(",", ".") + " --> " + str(sub.end).replace(",", ".") + "\n")
            vtt_file.write(sub.text + "\n\n")

    return vtt_path


def prepare_video_for_hls(video, vtt_path):
    print("Prepare video for hls...")

    video_m3u8_name = settings.MEDIA_ROOT + "videos/" + Path(video.video_file.path).stem + ".m3u8"
    ffmpeg_command = (f"ffmpeg -i {video.video_file.path} -codec: copy "
                      f"-start_number 0 -hls_time 10 -hls_list_size 0 -f hls {video_m3u8_name}")
    subprocess.run(shlex.split(ffmpeg_command))

    print("Prepare subtitles for hls...")

    vtt_m3u8_path = settings.MEDIA_ROOT + "videos/" + Path(vtt_path).stem + "_vtt.m3u8"
    with open(vtt_m3u8_path, mode="w") as vtt_m3u8_file:
        vtt_m3u8_file.write("#EXTM3U\n")
        vtt_m3u8_file.write("#EXT-X-VERSION:3\n")
        vtt_m3u8_file.write(f"#EXT-X-TARGETDURATION:1\n")
        vtt_m3u8_file.write("#EXT-X-MEDIA-SEQUENCE:1\n")
        vtt_m3u8_file.write("#EXTINF:1,\n")
        vtt_m3u8_file.write(Path(vtt_path).name)

    print("Write master m3u8 file...")

    master_m3u8_content = "#EXTM3U\n"
    master_m3u8_content += ("#EXT-X-MEDIA:TYPE=SUBTITLES,GROUP-ID=\"subs\",NAME=\"Deutsch\","
                            "DEFAULT=YES,AUTOSELECT=YES,FORCED=NO,LANGUAGE=\"de\","
                           "CHARACTERISTICS=\"public.accessibility.transcribes-spoken-dialog\","
                           f"URI=\"{Path(vtt_m3u8_path).name}\"\n")
    master_m3u8_content += "#EXT-X-STREAM-INF:SUBTITLES=\"subs\"\n"
    master_m3u8_content += Path(video.video_file.path).stem + ".m3u8"

    master_m3u8_file = ContentFile(master_m3u8_content, name=Path(vtt_path).stem + "_master.m3u8")
    video.m3u8_master = master_m3u8_file
    video.save()

    master_m3u8_file.close()
