import logging
import io
from pathlib import Path

import cv2
import numpy as np

from django.core.management.base import BaseCommand
from django.core.files import File

from core.models import Document, Token, Video
from core.helper import count_frames

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Reads an subtitle file"

    def add_arguments(self, parser):
        parser.add_argument("--video", help="video name")
        parser.add_argument("--video-file", help="video file")

    def handle(self, *args, **options):
        # Barcode generation is inspired by this tutorial:
        # https://pyimagesearch.com/2017/01/16/generating-movie-barcodes-with-opencv-and-python/
        print(options)
        if options["video"]:
            video_name = options["video"]
            video_obj = Video.objects.get(name=video_name)
        elif options["video_file"]:
            video_path = options["video_file"]
            with open(video_path, mode="rb") as video_file:
                video_file = File(video_file, name=Path(video_path).name)
                video_obj = Video.objects.create(name=Path(video_path).stem)
                video_obj.video_file = video_file
                video_obj.save()

        number_bars = 1000
        bar_width = 1
        barcode_height = 250

        if not video_obj.barcode_json:
            avgs = []
            total = 0

            print("Looping over frames in video (this will take awhile)...")

            total_frames = count_frames(video_obj.video_file.path)
            video = cv2.VideoCapture(video_obj.video_file.path)
            fps = video.get(cv2.CAP_PROP_FPS)

            skip_frames = int(total_frames / number_bars)

            # loop over the frames of the video
            while True:

                # grab the current frame
                (grabbed, frame) = video.read()

                # check to see if we have reached the end of the video
                if not grabbed:
                    break

                # increment the total number of frames read
                total += 1
                if total % 1000 == 0:
                    print(f"{total}/{total_frames}")

                # check to see if we should compute the average RGB value of the current frame
                if skip_frames == 0 or total % skip_frames == 0:
                    avg = cv2.mean(frame)[:3]
                    avgs.append(avg)

            # release the video pointer
            video.release()

            video_obj.barcode_json = avgs
            video_obj.total_length = int(total_frames / fps)
            video_obj.save()

        avgs = video_obj.barcode_json
        avgs = np.array(avgs, dtype="int")

        # grab the individual bar width and allocate memory for the barcode visualization
        bw = bar_width
        barcode = np.zeros((barcode_height, len(avgs) * bw, 3), dtype="uint8")

        # loop over the averages and create a single 'bar' for each frame average in the list
        for (i, avg) in enumerate(avgs):
            avg = [int(a) for a in avg]
            cv2.rectangle(barcode, (i * bw, 0), ((i + 1) * bw, barcode_height), tuple(avg), -1)

        # write the video barcode visualization to file and then display it to our screen
        _, im_buffer = cv2.imencode(".png", barcode)

        barcode_buffer = io.BytesIO(im_buffer)
        barcode_file = File(barcode_buffer, name=video_obj.name + "_barcode.png")
        video_obj.barcode_image = barcode_file
        video_obj.save()
        barcode_file.close()

