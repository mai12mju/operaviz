# OperaViz
OperaViz ist ein web-basiertes Visualisierungswerkzeug für Aufnahmen von Opernproduktionen, welches das Konzept von *scalable viewing* implementiert.

## Installation
Führe folgende Schritte aus, um OperaViz zu installieren:

1. Klone das git Repository auf dein System

2. Installiere ffmpeg (`https://ffmpeg.org/download.html`) und Python

3. Installiere die benötigen Python-Pakete mit dem Befehl: `pip install -r requirements.txt`

## Benutzung
Führe folgende Schritte aus, um OperaViz zu starten und neue Videos und Untertitel zur Verarbeitung in OperaViz zu laden. In dem Ordner `input_data` liegen Beispiel-Dateien von Operavision (`https://operavision.eu/de/performance/der-fliegende-hollaender`):

1. Lade das benötigte spacy model mit dem Befehl `python -m spacy download de_core_news_sm`

2. Erstelle die Datenbank-Tabellen mit dem Befehl `python manage.py migrate`

3. Starte den Backend Server mit dem Befehl `python manage.py runserver`

4. Lade die Video- und Untertieldateien mit dem Befehl `python manage.py import-video --sub input_data/holländer_mannheim_2022.srt --video input_data/holländer_mannheim_2022.mp4`

5. Generiere den Barcode mit dem Befehl `python manage.py generate-barcode --video holländer_mannheim_2022`

6. Rufe das Frontend in einem Browser unter `localhost:8000` auf
